# AnnotateBAMStatistics

This repository contains the code of AnnotateBAMStatistics. AnnotateBAMStatistics is a multi-threaded algorithm for annotating ANNOVAR output with additional useful fragment count statistics for one ore more specified BAM files. This includes fragment coverage, number of mutant fragments, fragment-based variant allele frequency and mutant read bias for high quality and all fragments separately.

## Installation instructions

AnnotateBAMStatistics takes an ANNOVAR file and one or more BAM files (comma-separated list) as input and adds informative fragment-based statistics. These statistics are generally used for filtering the variant list or to determine clonality in cancer samples. AnnotateBAMStatistics writes the output to standard output.

### Compile the binaries

First clone the GitHub repository with git:

```
git clone https://gitlab.com/erasmusmc-hematology/annotatebamstatistics
```

Then you need to compile the copy of samtools that comes packaged in this repository. This is necessary because AnnotateBAMStatistics relies on a specific version of samtools and may not work with newer versions.

```
cd annotatebamstatistics/samtools
make
```

Finally, go back to the root directory of AnnotateBAMStatistics and run make again:

```
make all
```

The binary AnnotateBAMStatistics is located at:

```bash
./dist/Release/GNU-Linux/AnnotateBAMStatistics
```

### Singularity container

The application has been containerized into a Singularity container and is available from Singularity hub. In case singularity is installed, simply run:

```bash
singularity pull shub://MathijsSanders/AnnotateBAMStatisticsSingularity
```

## How do I run it?

AnnotateBAMStatistics is a command-line utility that is run as follows:

`AnnotateBAMStatistics [options] > annovarFileOut.txt (output always to std::out)`

The following parameters are available:

- -a/--annovar-file*: Input ANNOVAR file
- -b/--bam-files*: One or more BAM files for which fragment statistics are calculated (comma-separated)
- -s/--min-base-score: Minimum base score for filtered statistics (Default=30)
- -S/--min-alignment-score: Minimum alignment score for filtered statistics (Default=40, based on BWA)
- -m/--min-match-length: Minimum alignment length for a read to be considered (Default=10)
- -t/--threads: Number of threads (Default 1)
- -d/--count-duplicates: If duplicates are used in the statistics (Default=false)
- -u/--count-supplementary: If supplementary alignments are used in the statistics (Default=false)
- -r/--pileup-regions: If individual regions from the annovar files are piled up. Otherwise scan the genome (Default=false)
- -v/--verbose: If specified be verbose (Default=false)
- -h/--help: Help information

*Dependencies*
- gcc 4.3+
- g++ 4.3+


## Acknowledgements & Citation

This program was written by Remco Hoogenboezem at Erasmus MC. Please cite this repository if you use this code in your research.
